# lsp_proxy

Tool for logging communication between an LSP Client and Server.

[https://microsoft.github.io/language-server-protocol/overviews/lsp/overview/](https://microsoft.github.io/language-server-protocol/overviews/lsp/overview/)

## Requirements

Works with client/server combinations where the server runs as a child process of the client and communication is done via stdin/stdout

## Usage

Configure `lsp_proxy` as a custom language server binary in your editor of choice with the folowing command line arguments:

```
Usage: <output_dir> <language_Server_command> [language_server_args]... 
Output: communication.txt       -> Annotated communication between client and server
        server_messages.txt     -> Server log messages
        client_to_server.txt    -> unaltered stream of data from client to server
        server_to_client.txt    -> unaltered stream of data from server to client
        lsp_inspector.log       -> log file compatible with Microsoft's LSP Inspector
Attention: old output files will be overwritten
```

Alternatively, you can use this tool without an editor and type lsp messages directly in your terminal; or read them from a file.

### Use case: Have a look at the communication between existing language clients and servers 

The official specification of the LSP can be hard to understand. If you are not sure about something, just use `lsp_proxy` to sniff the communication between someone elses client and server. Then have a look at `communication.txt` in the output directory to get a feel for the protocol. VSCode is a good choice to use as a client in this case, as it is basically driving the development of the protocol.

### Use case: Replay communication from an LSP Client

Use `lsp_proxy` to sniff the communication between an LSP Client and your own or another server and copy `client_to_server.txt` from the output directory to somewhere else. Then use the copied file as input for your language server like this:

```
$ my_language_server < client_to_server.txt
```

### Use case: Language Server Protocol Inspector

[https://github.com/microsoft/language-server-protocol-inspector](https://github.com/microsoft/language-server-protocol-inspector)

M$ made a handy tool called __Language Server Protocol Inspector__ that can visualize debug output from certain VSCode language client extensions. It seems like they don't support it anymore, but if you can manage to get it working it can be a great help to analyze the communication between a language client and a language server.

`lsp_proxy` can output log files compatible with their tool. That way you no longer need a VSCode extension to use it. Just open the `lsp_inspector.log` file from the output directory in the inspector.

## Issues

- This was developed as a quick and dirty debug tool and is probably riddled with bugs
- Only works on Linux and has not been tested thoroughly
- Server to client requests and the corresponding responces have not been tested at all
