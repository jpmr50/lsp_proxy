/*
Copyright (c) 2021 Jan Merle

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __BUFFER_H_
#define __BUFFER_H_

#include <stdint.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#define DECLARE_DYN_BUF(name, type) \
    typedef struct name##_s {       \
        size_t cap;                 \
        size_t len;                 \
        type *buf;                  \
    } name

// predefined dynamic buffer types

DECLARE_DYN_BUF(string_buf, char);
DECLARE_DYN_BUF(byte_buf, uint8_t);
DECLARE_DYN_BUF(int_buf, int);

// Generic macros to create and manipulate dynamic arrays in a typesafe manner
// Use DECLARE_DYN_BUF to make your own dynamic buffer types to use with these macros or use the predefined types
// NOTE: buffer and array are sometimes  used interchangeably in this file

// For byte buffers you can use the specialised functions byte_buffer_push and byte_buffer_append

// string_buf should only be used together with the specialised string_* functions. These always keep the strings
// null-terminated, so you can just use the buf member whenever you require a c string. If you have to manipulate the
// string without the string_* functions, you have to make sure that len == (strlen + 1) and cap >= len. In this case
// you can use BUF_MAYBE_GROW to manually increase the the buffer capacity.

// Pointers into a dynamic buffer may become invalid when the buffer needs to grow, so be careful if you do that
// TODO: maybe we could have a mechanism to "lock" the buffer, and have it assert if you try to grow a locked buffer

// BUF_NEW creates a new buffer of type t with an initial capacity n on the stack and returns it. This way you can
// do "int_buf b = BUF_NEW(int_buf, 10);".
// TODO: explanation why this is done like this
#define BUF_NEW(t, n) (*((t *)__buf_new((__void_buf *)&((t){0}), (n), sizeof(t))))

// BUF_PUSH takes a pointer to a buffer and an element, puts the element in the first free slot and returns the newly
// pushed element, growing the buffer when necessary. This can potentially invalidate all pointers pointing into the
// aray. 
// NOTE: b gets evaluated several times, so it has to be reentrant and side effect free. x is evaluated once and
// therefore can savely be any expression, e.g. "BUF_PUSH(b, get_random_value());"
#define BUF_PUSH(b, x) (__buf_grow((__void_buf *)(b), 1, sizeof(*((b)->buf))), (b)->buf[(b)->len++] = (x))

#define BUF_ADD(b, n) \
    (__buf_grow((__void_buf *)(b), (n), sizeof(*((b)->buf))), (b)->len += (n), &(b)->buf[(b)->len - (n)])

// TODO: currently this takes an increment, maybe use absolute size instead? Or maybe two macros for both?
#define BUF_MAYBE_GROW(b, inc) (__buf_grow((__void_buf *)(b), (inc), sizeof(*((b)->buf))))

// TODO: more

// byte buffer functions

void byte_buffer_push(byte_buf *buffer, uint8_t b);
uint8_t *byte_buffer_append(byte_buf *buffer, const uint8_t *const src, size_t n);

// string buffer functions

string_buf string_new(size_t initial_capacity);

void string_clear(string_buf *str);
void string_remove_leading(string_buf *str, size_t n);

void string_append_cstring_n(string_buf *dest, const char *const src, size_t n);
void string_append_cstring(string_buf *dest, const char *const src);
void string_append(string_buf *dest, const string_buf *const src);

void string_set_cstring_n(string_buf *dest, const char *const src, size_t n);
void string_set_cstring(string_buf *dest, const char *const src);
void string_set(string_buf *dest, const string_buf *const src);

void string_print_v(string_buf *output, const char *fmt, va_list args);
void string_print(string_buf *output, const char *fmt, ...);
void string_clearprint(string_buf *output, const char *fmt, ...);

// internal, don't use
DECLARE_DYN_BUF(__void_buf, void);
__void_buf *__buf_new(__void_buf *buf, size_t initial_capacity, size_t elem_size);
void __buf_grow(__void_buf *buf, size_t inc, size_t elem_size);

/*
// // NOTE: unfortunately BUF_APPEND is not typesafe, so be careful if you use it or wrap it in a function for your
// // specific buffer type, like byte_buffer_append
// // TODO: can we generate a wrapper function with a macro?
// #define BUF_APPEND(b, arr, n) (memcpy(BUF_ADD((b), (n)), (arr), (n) * sizeof(*((b)->buf))))

// // TODO: to make the following work, we need something like a BUFFER_IMPL symbol which has to be defined in only one
// // compilation unit to prevent multiple definitions of the generated wrapper functions

// #ifdef BUFFER_IMPL
// #define DECLARE_DYN_BUF_WITH_WRAPPER(name, type)                                    \
//     struct name##_s {                                                               \
//         size_t cap;                                                                 \
//         size_t len;                                                                 \
//         type *buf;                                                                  \
//     };                                                                              \
//     type *name##_append(struct name##_s *buffer, const type *const src, size_t n) { \
//         return BUF_APPEND(buffer, src, n);                                          \
//     }                                                                               \
//     struct name##_s name##_new(size_t initial_capacity) {                           \
//         struct name##_s result = {0};                                               \
//         __buf_new((__void_buf *)&result, initial_capacity, sizeof(*(result.buf)));  \
//         return result;                                                              \
//     }                                                                               \
//     typedef struct name##_s name
// #else
// #define DECLARE_DYN_BUF_WITH_WRAPPER(name, type)
//     struct name##_s {                                                              \
//         size_t cap;                                                                \
//         size_t len;                                                                \
//         type *buf;                                                                 \
//     };                                                                             \
//     type *name##_append(struct name##_s *buffer, const type *const src, size_t n); \
//     struct name##_s name##_new(size_t initial_capacity);                           \
//     typedef struct name##_s name
// #endif

// DECLARE_DYN_BUF_WITH_WRAPPER(byte_buf, uint8_t);

// void byte_buffer_push(byte_buf *buffer, uint8_t b);
// // uint8_t *byte_buf_append(byte_buf *buffer, const uint8_t *const src, size_t n);

*/

#endif
