/*
Copyright (c) 2021 Jan Merle

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include <assert.h>
#include <stdbool.h>

#include "buffer.h"
#include "cJSON.h"

#include <signal.h>
#include <sys/wait.h>
#include <sys/resource.h>
#include <sys/types.h>

#include <time.h>
#include <sys/time.h>

#define STB_DS_IMPLEMENTATION
#include "stb_ds.h"

#define READ 0
#define WRITE 1

typedef enum {
    STATE_READ_HEADER,
    STATE_HEADER_END_A,
    STATE_HEADER_END_B,
    STATE_HEADER_END_C,
    STATE_READ_CONTENT
} lsp_fsm_state_t;

typedef enum { SENDER_CLIENT, SENDER_SERVER } lsp_sender_t;

/**
 * value type for request hash map
 */
typedef struct {
    char *method;
    struct timeval time;
} lsp_request_info_val_t;

/**
 * kv struct for request hash map
 */
typedef struct {
    double key;
    lsp_request_info_val_t value;
} lsp_request_info_t;

/**
 * Data for a finite state machine used to parse LSP Messages.
 *
 * The reason we use a state machine for parsing is to be able
 * to parse messages from the client and the server concurrently.
 * Characters are read from non-blocking input streams and are
 * used to advance the state machine. This process is repeated
 * as long as the server is still running, alternating between
 * the state machine of the client and the server.
 *
 * @remark Here, parsing means only parsing the header part of
 *         an LSP message and collecting the content. Parsing
 *         the content string into a json object for further
 *         analysis is done via the cJSON library.
 *         See also @ref log_for_lsp_inspector
 *
 * @see new_state_machine
 * @see lsp_consume_char
 */
typedef struct {
    lsp_fsm_state_t state;
    size_t header_line_start;
    int content_length;
    int content_consumed;
    FILE *file;
    string_buf buffer;
    lsp_request_info_t **request_map_put;
    lsp_request_info_t **request_map_get;
    int io_in;
    int io_out;
    lsp_sender_t sender;
    int counter;
} lsp_fsm_t;

typedef struct {
    FILE *communication_file;
    FILE *inspector_file;
    int total_message_counter;
    string_buf inspector_buffer;
} shared_data_t;

// global data for handle_sigchld
static pid_t g_server_pid = 0;
static volatile bool g_run = true;
static bool g_sigchld = false;
static int g_status = 0;

/**
 * Initialize a new state machine for LSP message parsing
 *
 * @param file          output file for the sender's raw messages
 * @param io_in         input stream (read from sender)
 * @param io_out        output stream (forward to receiver)
 * @param sender        sender of the messages parsed by the new state machine
 * @return              the new state machine
 */
static lsp_fsm_t new_state_machine(lsp_sender_t sender, FILE *file, int io_in, int io_out,
                                   lsp_request_info_t **request_map_put, lsp_request_info_t **request_map_get);

/**
 * Try to read a character from the sender side, forward it to the receiver
 * side and advance the state machine.
 *
 * Abstract summary of the state machine:
 *   1. Parse the header
 *   2. Read the rest of the message
 *   3. Analyze and log the message
 *   4. Goto 1.
 *
 * @param fsm           the state machine
 * @param shared        auxilliary data, shared between all state machines
 * @return              false if fsm->sender == SERVER and fsm->io_in reached EOF,
 *                      true otherwise
 */
static bool lsp_consume_char(lsp_fsm_t *fsm, shared_data_t *const shared);

/**
 * Analyse json-rpc message and log to file in an LSP Inspector compatible format
 *
 * @param sender            sender of the lsp message
 * @param message           content of an lsp message (without the header part)
 * @param time_reveived     time the message was received
 * @param shared            auxilliary data, shared between all state machines
 */
static void log_for_lsp_inspector(lsp_sender_t sender, char *message, lsp_request_info_t **request_map_send,
                                  lsp_request_info_t **request_map_receive, struct timeval time_reveived,
                                  FILE *inspector_file, string_buf *inspector_buffer);

/**
 * Detect server shutdown
 *
 * @param sig       Hopefully a SIGCHLD signal; ignored
 */
static void handle_sigchld(int sig) {
    (void)sig;
    int tmp_errno = errno;

    while (g_sigchld == false) {
        int pid = waitpid(-1, &g_status, WNOHANG);
        if (pid == g_server_pid) {
            g_run = false;
            g_sigchld = true;
        }
        break;
    }

    errno = tmp_errno;
}

// tested with:
// clangd -log=verbose --background-index
// zls

int main(int argc, char **argv) {

    if (argc < 3) {
        printf("Usage: <output_dir> <language_Server_command> [language_server_args]... \n");
        printf("Output: communication.txt       -> Annotated communication between client and server\n");
        printf("        server_messages.txt     -> Server log messages\n");
        printf("        client_to_server.txt    -> unaltered stream of data from client to server\n");
        printf("        server_to_client.txt    -> unaltered stream of data from server to client\n");
        printf("        lsp_inspector.log       -> log file compatible with Microsoft's LSP Inspector\n");
        printf("Attention: old output files will be overwritten\n");
        exit(0);
    }

    // Set up sigchld handler (for figuring out when and how the server got terminated)
    struct sigaction siga;
    siga.sa_handler = handle_sigchld;
    siga.sa_flags = SA_RESTART; // SA_NOCLDSTOP
    sigemptyset(&siga.sa_mask);
    if (sigaction(SIGCHLD, &siga, NULL) == -1) {
        perror("LSP_PROXY: Error setting up sigchld handler\n");
    }

    // Set up pipes for communication
    //
    // client (stdin) -> lsp_proxy -> server (pipe_out)
    // client (stdout) <- lsp_proxy <- server (pipe_in)
    //
    // The pipe names are relative to the parent:
    // The parent writes into pipe_out[WRITE] and reads from pipe_in[READ]
    // For the child process they have the opposite meaning:
    // The child writes into pipe_in[WRITE] and reads from pipe_out[READ]

    int pipe_out[2] = {0};
    int pipe_in[2] = {0};
    int pipe_err[2] = {0};

    int result;

    result = pipe(pipe_out);
    if (result == -1) {
        perror("LSP_PROXY: create pipe_out error:");
        exit(-1);
    }

    result = pipe(pipe_in);
    if (result == -1) {
        perror("LSP_PROXY: create pipe_in error:");
        exit(-1);
    }

    result = pipe(pipe_err);
    if (result == -1) {
        perror("LSP_PROXY: create pipe_err error:");
        exit(-1);
    }

    // Set input streams to non-blocking
    fcntl(pipe_in[READ], F_SETFL, fcntl(pipe_in[READ], F_GETFL, 0) | O_NONBLOCK);  // TODO: error check
    fcntl(STDIN_FILENO, F_SETFL, fcntl(STDIN_FILENO, F_GETFL, 0) | O_NONBLOCK);    // TODO: error check
    fcntl(pipe_err[READ], F_SETFL, fcntl(pipe_in[READ], F_GETFL, 0) | O_NONBLOCK); // TODO: error check

    // fork and start the server in the child process
    pid_t pid = fork();
    if (pid == 0) {
        // CHILD setup:

        signal(SIGCHLD, SIG_DFL);

        // setup pipes as standard streams for the server
        // NOTE: since this is the child, pipe_out and pipe_in mean the opposite

        while (((result = dup2(pipe_out[READ], STDIN_FILENO)) == -1) && (errno == EINTR)) {
        }
        if (result == -1) {
            perror("LSP_PROXY: dup2 pipe_out error:");
            exit(-1);
        }

        while (((result = dup2(pipe_in[WRITE], STDOUT_FILENO)) == -1) && (errno == EINTR)) {
        }
        if (result == -1) {
            perror("LSP_PROXY: dup2 pipe_in error:");
            exit(-1);
        }

        while (((result = dup2(pipe_err[WRITE], STDERR_FILENO)) == -1) && (errno == EINTR)) {
        }
        if (result == -1) {
            perror("dup2 pipe_err error:");
            exit(-1);
        }

        // start the server

        char **command = argv + 2;
        if (execvp(command[0], command) == -1) {
            perror("LSP_PROXY: execvp error:");
        }

        exit(-1);

    } else if (pid < 0) {
        perror("LSP_PROXY: failed to fork");
        exit(-1);
    }

    // PARENT setup:

    g_server_pid = pid;

    shared_data_t shared = {0};

    char *output_dir = argv[1];
    string_buf sb = string_new(1024 * 1024);

    // Hacky, but works for now
    string_clearprint(&sb, "mkdir -p %s", output_dir);
    system(sb.buf);

    string_clearprint(&sb, "%s%s", output_dir, "/communication.txt");
    shared.communication_file = fopen(sb.buf, "w");

    string_clearprint(&sb, "%s%s", output_dir, "/server_messages.txt");
    FILE *log_file = fopen(sb.buf, "w");

    string_clearprint(&sb, "%s%s", output_dir, "/client_to_server.txt");
    FILE *client_to_server_file = fopen(sb.buf, "w");

    string_clearprint(&sb, "%s%s", output_dir, "/server_to_client.txt");
    FILE *server_to_client_file = fopen(sb.buf, "w");

    string_clearprint(&sb, "%s%s", output_dir, "/lsp_inspector.log");
    shared.inspector_file = fopen(sb.buf, "w");

    lsp_request_info_t *request_map_client_to_server = NULL;
    lsp_request_info_t *request_map_server_to_client = NULL;

    lsp_fsm_t client_fsm = new_state_machine(SENDER_CLIENT, client_to_server_file, STDIN_FILENO, pipe_out[WRITE],
                                             &request_map_client_to_server, &request_map_server_to_client);
    lsp_fsm_t server_fsm = new_state_machine(SENDER_SERVER, server_to_client_file, pipe_in[READ], STDOUT_FILENO,
                                             &request_map_server_to_client, &request_map_client_to_server);

    string_clear(&sb);
    shared.inspector_buffer = sb;
    sb.buf = NULL;
    shared.total_message_counter = 0;

    string_buf log_buffer = string_new(1024);

    // main loop:
    //   (reading does not block)
    //   1. try to read char from client and process it
    //   2. try to read char from server and process it
    //   3. try to read char from server (stderr) and process it
    //   4. goto 1.

    // TODO: this is burning cpu, maybe a short sleep wouldn't hurt?
    g_run = true;
    while (g_run) {
        // from client
        if (!lsp_consume_char(&client_fsm, &shared)) {
            g_run = false;
        }

        // from server
        if (!lsp_consume_char(&server_fsm, &shared)) {
            g_run = false;
        }

        // from server (stderr)
        char c;
        int num_read = read(pipe_err[READ], &c, 1);
        if (num_read == 1) {
            string_append_cstring_n(&log_buffer, &c, 1);

            if (c == '\n') {
                fprintf(log_file, "%s", log_buffer.buf);
                fprintf(stderr, "%s", log_buffer.buf);
                fflush(log_file);
                fflush(stderr);
                string_clear(&log_buffer);
            }
        }
    }

    // server terminated, figure out why
    int ret = 0;
    if (g_sigchld) {
        if (WIFEXITED(g_status)) {
            fprintf(stderr, "LSP_PROXY: Language Server terminated normally with exit code %d\n",
                    WEXITSTATUS(g_status));
            ret = WEXITSTATUS(g_status);
        } else if (WIFSIGNALED(g_status)) {
            int sig = WTERMSIG(g_status);
            const char *sig_name = sigdescr_np(sig);
            fprintf(stderr, "LSP_PROXY: Language Server got terminated by signal %s (%d).",
                    sig_name != NULL ? sig_name : "", sig);
            if (WCOREDUMP(g_status)) {
                fprintf(stderr, "Core dumped\n");
            } else {
                fprintf(stderr, "\n");
            }
            ret = 128 + WTERMSIG(g_status);
        }
    }
    fflush(stderr);

    // cleanup

    fclose(shared.communication_file);
    fclose(log_file);
    fclose(client_fsm.file);
    fclose(server_fsm.file);
    fclose(shared.inspector_file);

    hmfree(request_map_client_to_server);
    hmfree(request_map_server_to_client);
    free(client_fsm.buffer.buf);
    free(server_fsm.buffer.buf);
    free(shared.inspector_buffer.buf);
    free(log_buffer.buf);

    return ret;
}

static lsp_fsm_t new_state_machine(lsp_sender_t sender, FILE *file, int io_in, int io_out,
                                   lsp_request_info_t **request_map_put, lsp_request_info_t **request_map_get) {
    lsp_fsm_t result = {0};

    result.state = STATE_READ_HEADER;
    result.header_line_start = 0;
    result.content_length = -1;
    result.content_consumed = 0;
    result.file = file;
    result.buffer = string_new(1024 * 1024);
    result.request_map_put = request_map_put;
    result.request_map_get = request_map_get;
    result.io_in = io_in;
    result.io_out = io_out;
    result.sender = sender;
    result.counter = 0;

    return result;
}

static bool lsp_consume_char(lsp_fsm_t *fsm, shared_data_t *const shared) {

    char c;
    int num_read;

    // TODO: repeat in case of EAGAIN, EINTR?
    num_read = read(fsm->io_in, &c, 1);
    if (num_read == 0) {

        if (fsm->sender == SENDER_CLIENT) {
            // fprintf(stderr, "got EOF from client\n");
        } else {
            fprintf(stderr, "LSP_PROXY: got EOF from server\n");
            return false;
        }
    } else if (num_read == 1) {
        write(fsm->io_out, &c, 1);
        fputc(c, fsm->file);
        fflush(fsm->file);
        string_append_cstring_n(&fsm->buffer, &c, 1);

        switch (fsm->state) {
        case STATE_READ_HEADER: {
            if (c == '\r' || c == '\n') {
                if (c == '\r') {
                    fsm->state = STATE_HEADER_END_A;
                } else if (c == '\n') {
                    fsm->state = STATE_HEADER_END_C;
                }

                int tmp_len = 0;
                int result = sscanf(fsm->buffer.buf + fsm->header_line_start, "Content-Length: %d", &tmp_len);
                fsm->content_length = result == 1 ? tmp_len : -1;
            }
        } break;
        case STATE_HEADER_END_A: {
            if (c == '\n') {
                fsm->state = STATE_HEADER_END_B;
            } else {
                fsm->state = STATE_READ_HEADER;
                fsm->header_line_start = fsm->buffer.len - 1;
            }
        } break;
        case STATE_HEADER_END_B: {
            if (c == '\r') {
                fsm->state = STATE_HEADER_END_C;
            } else {
                fsm->state = STATE_READ_HEADER;
                fsm->header_line_start = fsm->buffer.len - 1;
            }
        } break;
        case STATE_HEADER_END_C: {
            if (c == '\n') {
                if (fsm->content_length != -1) {
                    fsm->state = STATE_READ_CONTENT;
                } else {
                    fsm->state = STATE_READ_HEADER;
                }
            } else {
                fsm->state = STATE_READ_HEADER;
                fsm->header_line_start = fsm->buffer.len - 1;
            }
        } break;
        case STATE_READ_CONTENT: {
            // assert(state->content_length != -1);
            ++fsm->content_consumed;

            if (fsm->content_consumed == fsm->content_length) {
                fsm->state = STATE_READ_HEADER;

                char *content = fsm->buffer.buf + (fsm->buffer.len - fsm->content_length - 1);

                fsm->header_line_start = 0;
                fsm->content_length = -1;
                fsm->content_consumed = 0;

                ++shared->total_message_counter;
                ++fsm->counter;

                // https://codereview.stackexchange.com/questions/11921/getting-current-time-with-milliseconds
                struct timeval time_reveived;
                gettimeofday(&time_reveived, NULL);
                long millis = time_reveived.tv_usec / 1000;
                char time_buffer[80];
                strftime(time_buffer, 80, "%Y-%m-%d %H:%M:%S", localtime(&time_reveived.tv_sec));

                if (fsm->sender == SENDER_CLIENT) {
                    fprintf(shared->communication_file, "\n>>>>>>>>>>>>>>>>>>[%s:%03ld] client: %06d / total: %06d >>>>>>>>>>>>\n",
                            time_buffer, millis, fsm->counter, shared->total_message_counter);
                } else {
                    fprintf(shared->communication_file, "\n<<<<<<<<<<<<<<<<<<[%s:%03ld] server: %06d / total: %06d <<<<<<<<<<<<\n",
                            time_buffer, millis, fsm->counter, shared->total_message_counter);
                }

                log_for_lsp_inspector(fsm->sender, content, fsm->request_map_put, fsm->request_map_get, time_reveived,
                                      shared->inspector_file, &shared->inspector_buffer);

                // fprintf(stdout, "\n########################## Writing file from %d\n", state->sender);
                // fprintf(stdout, "\n########################## Request state %d\n", g_req->state);
                // fflush(stdout);

                fprintf(shared->communication_file, "%s", fsm->buffer.buf);
                fflush(shared->communication_file);

                string_clear(&fsm->buffer);
            }
        } break;
        default:
            assert(false);
        }
    }

    return true;
}

static void log_for_lsp_inspector(lsp_sender_t sender, char *message, lsp_request_info_t **request_map_put,
                                  lsp_request_info_t **request_map_get, struct timeval time_reveived,
                                  FILE *inspector_file, string_buf *inspector_buffer) {

    // TODO: check for all the things that could go wrong in this function

    string_clear(inspector_buffer);
    cJSON *json = cJSON_Parse(message);

    if (json == NULL) {
        return;
    }

    cJSON *params = cJSON_GetObjectItemCaseSensitive(json, "params");
    cJSON *result = cJSON_GetObjectItemCaseSensitive(json, "result");
    cJSON *id = cJSON_GetObjectItemCaseSensitive(json, "id");
    cJSON *method = cJSON_GetObjectItemCaseSensitive(json, "method");

    char time_buffer[80];
    strftime(time_buffer, 80, "%I:%M:%S %p", localtime(&time_reveived.tv_sec));

    // Server or Client?
    string_print(inspector_buffer, "[Trace - %s] %s ", time_buffer, sender == SENDER_CLIENT ? "Sending" : "Received");

    // Notification or Request or Response?
    if (cJSON_IsString(method) && (method->valuestring != NULL) && (id == NULL)) {
        // Notification
        string_print(inspector_buffer, "notification '%s'.\n", method->valuestring);
    } else if (cJSON_IsString(method) && (method->valuestring != NULL) && (id != NULL)) {
        // Request
        char *id_string = cJSON_Print(id);
        string_print(inspector_buffer, "request '%s - (%s)'.\n", method->valuestring, id_string ? id_string : "");

        // TODO: technically id could also be a string or NULL. We don't support that currently.
        if (cJSON_IsNumber(id)) {

            // Remember time and method name for request id. We need this, because the
            // LSP Inspector expects responses to have a name and a duration.
            char *method_name = malloc(strlen(method->valuestring) + 1);
            strcpy(method_name, method->valuestring);
            double id_num = cJSON_GetNumberValue(id);
            lsp_request_info_val_t value = {method_name, time_reveived};
            hmput(*request_map_put, id_num, value);
        }

        free(id_string);

    } else if ((method == NULL) && (id != NULL)) {
        // Response
        char *id_string = cJSON_Print(id);
        char *method_name = NULL;
        long ms = 0;

        // TODO: technically id could also be a string or NULL. We don't support that currently.
        if (cJSON_IsNumber(id)) {
            double id_num = cJSON_GetNumberValue(id);

            // Get name and start time of the corresponding request (if possible)
            ptrdiff_t index = hmgeti(*request_map_get, id_num);
            if (index > -1) {
                lsp_request_info_val_t value = (*request_map_get)[index].value;
                ms = (time_reveived.tv_sec * 1000 + time_reveived.tv_usec / 1000) -
                     (value.time.tv_sec * 1000 + value.time.tv_usec / 1000);
                method_name = value.method;
                int ret = hmdel(*request_map_get, id_num);
                (void)ret;
            }
        }

        string_print(inspector_buffer, "response '%s - (%s)'", method_name ? method_name : "unknown",
                     id_string ? id_string : "");

        if (SENDER_CLIENT) {
            string_print(inspector_buffer, ". Processing request took %ldms\n", ms);
        } else {
            string_print(inspector_buffer, " in %ldms.\n", ms);
        }

        free(method_name);
        free(id_string);
    }

    // Notification/Request with Parameters or Response with Result?
    if (params) {
        char *params_string = cJSON_Print(params);
        string_print(inspector_buffer, "Params: %s", params_string);
        free(params_string);
    } else if (result) {
        char *result_string = cJSON_Print(result);
        string_print(inspector_buffer, "Result: %s", result_string);
        free(result_string);
    }

    fprintf(inspector_file, "%s\n\n\n", inspector_buffer->buf);
    fflush(inspector_file);

    cJSON_Delete(json);
}
