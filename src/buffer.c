/*
Copyright (c) 2021 Jan Merle

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "buffer.h"
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

#ifdef NDEBUG
#define CHECK_STRING_INVARIANT(str) ((void)0)
#else
#define CHECK_STRING_INVARIANT(str)     \
    do {                                \
        const string_buf *_str = (str); \
        assert(_str);                   \
        assert(_str->len >= 1);         \
        assert(_str->len <= _str->cap); \
        assert(_str->buf);              \
    } while (0)
#endif

#define MAX(x, y) ((x) >= (y) ? (x) : (y))

// don't use __buf_new and __buf_grow directly, use BUF_* macros instead

__void_buf *__buf_new(__void_buf *buf, size_t initial_capacity, size_t elem_size) {
    assert(buf);
    buf->cap = initial_capacity;
    buf->len = 0;
    buf->buf = malloc(initial_capacity * elem_size);
    return buf;
}

void __buf_grow(__void_buf *buf, size_t inc, size_t elem_size) {
    assert(buf);
    assert(buf->buf);
    assert(buf->cap >= buf->len);

    size_t required_cap = buf->len + inc;
    if (required_cap <= buf->cap) {
        return;
    }

    size_t new_cap = MAX(1 + 2 * buf->cap, required_cap);

    assert(required_cap <= new_cap);

    // TODO: overflow
    size_t new_size_in_bytes = new_cap * elem_size;

    buf->buf = realloc(buf->buf, new_size_in_bytes);

    // TODO: this is allowed to fail, so don't just assert
    assert(buf->buf != NULL);

    buf->cap = new_cap;
}

// byte buffer functions

void byte_buffer_push(byte_buf *buffer, uint8_t by) { BUF_PUSH(buffer, by); }

uint8_t *byte_buffer_append(byte_buf *buffer, const uint8_t *const src, size_t n) {
    uint8_t *buffer_data_append = BUF_ADD(buffer, n);
    return memcpy(buffer_data_append, src, n);
}

// string buffer functions

string_buf string_new(size_t initial_capacity) {
    string_buf result = {0};
    if (initial_capacity < 1) {
        initial_capacity = 1;
    }
    result = BUF_NEW(string_buf, initial_capacity);
    BUF_PUSH(&result, '\0');
    return result;
}

void string_clear(string_buf *str) {
    CHECK_STRING_INVARIANT(str);
    str->len = 1;
    *str->buf = 0;
}

void string_remove_leading(string_buf *str, size_t n) {
    // TODO: this might be too slow and we have to come up with something smarter
    CHECK_STRING_INVARIANT(str);
    assert(str->len > n); // with null character
    if (n == 0) {
        return;
    }

    str->len = str->len - n;

    // TODO: what is faster, looping or memmove? (my guess is memmove, but we should still profile it)
    memmove(str->buf, str->buf + n, str->len);
    // for (int i = 0; i < buffer->len; ++i) {
    //     buffer->data[i] = buffer->data[i + n];
    // }

    assert(str->buf[str->len - 1] == 0);
}

// TODO: we have 4 choices: pass number of chars with \0, without \0 (current), or pass a pointer to the \0 byte or pass
// a pointer to the character right before \0
// TODO: use memmove instead of memcpy?
void string_append_cstring_n(string_buf *dest, const char *const src, size_t n) {
    CHECK_STRING_INVARIANT(dest);

    char *str_data_append = BUF_ADD(dest, n) - 1;
    assert(str_data_append >= dest->buf);

    memcpy(str_data_append, src, n);

    *(dest->buf + dest->len - 1) = 0;
}

void string_append_cstring(string_buf *dest, const char *const src) { string_append_cstring_n(dest, src, strlen(src)); }

void string_append(string_buf *dest, const string_buf *const src) {
    CHECK_STRING_INVARIANT(src);
    string_append_cstring_n(dest, src->buf, src->len - 1);
}

void string_set_cstring_n(string_buf *dest, const char *const src, size_t n) {
    string_clear(dest);
    string_append_cstring_n(dest, src, n);
}

void string_set_cstring(string_buf *dest, const char *const src) { string_set_cstring_n(dest, src, strlen(src)); }

void string_set(string_buf *dest, const string_buf *const src) {
    CHECK_STRING_INVARIANT(src);
    string_set_cstring_n(dest, src->buf, src->len - 1);
}

void string_print_v(string_buf *output, const char *fmt, va_list args) {
    va_list args_copy;

    assert(output->buf != NULL && output->cap != 0);
    CHECK_STRING_INVARIANT(output);

    va_copy(args_copy, args);
    size_t space_needed = vsnprintf(NULL, 0, fmt, args_copy);
    va_end(args_copy);

    char *str_data_append = BUF_ADD(output, space_needed) - 1;
    assert(str_data_append >= output->buf);

    // va_start(args, fmt);
    vsnprintf(str_data_append, space_needed + 1, fmt, args);
    // va_end(args);

    assert(output->len <= output->cap);
}

void string_print(string_buf *output, const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    string_print_v(output, fmt, args);
    va_end(args);
}

void string_clearprint(string_buf *output, const char *fmt, ...) {
    va_list args;
    string_clear(output);
    va_start(args, fmt);
    string_print_v(output, fmt, args);
    va_end(args);
}
